import numpy as np

x = np.array([1, 2])
y = np.array([3, 4])

f = open("test_python.txt", "a")
f.write(str(np.dot(x.T, y)) + "\n")
f.write("numpy is working, test script pass \n")
f.close()

print("see if this text show in the log file")