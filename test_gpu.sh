#!/bin/bash

### sbatch config parameters must start with #SBATCH, to ignore just add another # - like ##SBATCH

#SBATCH --partition debug						### specify partition name where to run a job

##SBATCH --time 0-3:00:00						### limit the time of job running, partition limit can override this

#SBATCH --job-name python_test						### name of the job
#SBATCH --output /home/dorlit/python_test_log.log			### output log for running job - %J for job number
##SBATCH --mail-user=dorlit@post.bgu.ac.il				### users email for sending job status
#SBATCH --mail-type=ALL							### conditions when to send the email

#SBATCH --gres=gpu:1							### number of GPUs, ask for more than 1 only if you can parallelize your code for multi GPU

### Start you code below ####

module load anaconda							### load anaconda module
source activate python							### activating environment, environment must be configured before running the job (conda)
python python_test.py							### execute jupyter lab command
